# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 14:11:45 2021

@author: DECUREY
"""
def guipure():
    '''
    Renvoi la valeur des billets à rendre selon le choix de l'utilisateur et
    le nombre de billet dans la caisse
    Entrée : None
    Sortie : Valeurs et nombre de billets à rendre
    '''

    caisse_guipure = [[200, 1], [100, 3], [50, 1], [20, 1], [10, 1], [2, 5]]
    valeurs_billets = [[200, 0], [100, 0], [50, 0], [20, 0], [10, 0], [2, 5]]
    monnaie_2 = int(input("Quelle valeur doit-on rendre ? : "))

    if monnaie_2 == 0:
            print('\n  La somme doit être supérieure à 0')

    if monnaie_2 >= 590:
        print("\n  La somme ne doit pas être supérieure à 590 euros")
    elif monnaie_2 < 0:
        print('\n  La somme ne doit pas être inférieure à 0')
    else:

        for j in range(len(caisse_guipure)):
            i = caisse_guipure[j]
            if monnaie_2 >= i[0]:
                ent = monnaie_2 // i[0]
                if ent > i[1]:
                    print(f"rendre {i[1]} billets de {i[0]}€")
                    monnaie_2 = monnaie_2-i[1] * i[0]
                    valeurs_billets[j][1] += i[1]
                else:
                    print(f"rendre {i[1]} billets de {i[0]}€")
                    monnaie_2 = monnaie_2-ent * i[0]
                    valeurs_billets[j][1] += ent
    
        if monnaie_2 != 0:
            print("Somme rendue différente")
        return valeurs_billets

guipure()


