# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 10:28:29 2021

@author: DECUREY
"""


def fleury_bott():
    caisse = [500, 200 , 50, 20, 10, 5, 2, 1]
    monnaie = int(input("Quelle valeur doit-on rendre ? : "))
    if monnaie > 0:
        for i in range(len(caisse)):
            if monnaie >= caisse[i]:
                print(f'il faut rendre {monnaie // caisse[i]} billet(s) de {caisse[i]}')
                monnaie = monnaie % caisse[i]
        return monnaie
    else:
        print("La somme doit être supérieure à 0")
    
fleury_bott()
