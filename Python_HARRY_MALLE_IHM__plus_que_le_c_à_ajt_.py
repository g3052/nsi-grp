# -- coding: utf-8 --

# partie 1 : joue un tour de jeu aléatoire


choix_utilisateur = str(input("Dans quelle boutique voulez vous aller ? : "))
    
if choix_utilisateur == "Fleury et Bott":
    
    def fleury_bott():
        caisse = [500, 200 , 50, 20, 10, 5, 2, 1]
        monnaie = int(input("Quelle valeur doit-on rendre ? : "))
        if monnaie > 0:
            for i in range(len(caisse)):
                if monnaie >= caisse[i]:
                    print(f'il faut rendre {monnaie // caisse[i]} billet(s) de {caisse[i]}')
                    monnaie = monnaie % caisse[i]
            return monnaie
        else:
            print("La somme doit être supérieure à 0")

    fleury_bott()

elif choix_utilisateur == "Madame Guipure":
    
    def guipure():
        
        caisse_2 = {200 : 1, 100 : 3, 50 : 1, 20 : 1, 10 : 1, 2 : 5}
        monnaie_2 = int(input("Quelle valeur doit-on rendre ? : "))
        
        
        if monnaie_2 == 0:
                print('La somme doit être supérieure à 0')
                
        
        for valeur_billets in caisse_2.keys():
            if monnaie_2 >= 590:
                print("La somme ne doit pas être supérieure à 590 euro")
                break
            elif monnaie_2 <0:
                print('La somme ne doit pas être inférieure à 0')
                break
            elif monnaie_2 == 0: 
                break
            
            else:
                 print(f'il faut rendre {monnaie_2 // valeur_billets} billet(s) ou pièce(s) de {valeur_billets}')
                 monnaie_2 = monnaie_2 % valeur_billets

            if monnaie_2 % valeur_billets == 1 :
                print('La somme rendue est différente')
                
        return valeur_billets
    guipure()
    
elif choix_utilisateur == "Olivander":
        
else:
    print("Veuillez saisir une autre boutique")
