# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 13:14:17 2021

@author: FAUREP
"""


def fleury_bott(somme_a_rendre ):
    caisse = [500, 200 , 100, 50, 20, 10, 5, 2, 1]

    
    #somme_a_rendre = int(input("Quelle valeur doit-on rendre ? : "))
    if somme_a_rendre > 0:
        
        for i in range(len(caisse)):
            
            if somme_a_rendre >= caisse[i]:
                print(f'  Il faut rendre {somme_a_rendre // caisse[i]} billet(s) de {caisse[i]}')
                somme_a_rendre = somme_a_rendre % caisse[i]
        
        return somme_a_rendre

    else:
            print("\n  La somme doit être supérieure à 0 ")

    


def fleury_bott_automatique(somme_test) : 
  
    for somme_a_rendreu in somme_test:
        print(f"Pour une somme à rendre de {somme_a_rendreu} € :")
        fleury_bott(somme_a_rendreu)
    somme_a_rendre_choix = int(input("Quelle valeur voulez vous rendre ? : "))
    fleury_bott(somme_a_rendre_choix)

def guipure():
    
    

    caisse_guipure=[[200,1, 'noise'],[100,3],[50,1],[20,1],[10,1],[2,5]]
    valeurs_billets=[[200,0],[100,0],[50,0],[20,0],[10,0],[2,5]]
    monnaie_2 = int(input("Quelle valeur doit-on rendre ? : "))
    
    
    if monnaie_2 == 0:
            print('\n__La somme doit être supérieure à 0__')
            
    
    if monnaie_2 >= 590:
      print("\n__La somme ne doit pas être supérieure à 590 euro__")
    elif monnaie_2 <0:
      print('\n__La somme ne doit pas être inférieure à 0__')
    else:
      
      for j in range(len(caisse_guipure)):
        i=caisse_guipure[j]
        if monnaie_2>=i[0]:
           ent=monnaie_2//i[0]
           if ent>i[1]:
              print(f"rendre {i[1]}  billets de {i[0]}")
              monnaie_2=monnaie_2-i[1]*i[0]
              valeurs_billets[j][1]+=i[1]
           else:
              print(f"rendre {ent} billets de {i[0]} €")
              monnaie_2=monnaie_2-ent*i[0]
              valeurs_billets[j][1]+=ent
            

            
      #  if monnaie_2 % valeur_billets == 1 :
       #     print('La somme est rendue est différente')
    if monnaie_2!=0:
      print("pas assez de billets")  
    return valeurs_billets
            
    guipure()

    

def ollivender():
    print ("là il faut mettre le code pour la boutique d ollivender")
   

#Cette fonction permet à l'utilisateur de choisir si il veut continuer ou arrêter le programme, elle fait partie de l'ihm       
def arreter_ou_continuer_le_programme():
    global test #on change une variable globale dans une fonction  
    test=False
    arret_continuation = input("Voulez aller dans une autre boutique ?"\
          "\nPour aller dans une autre boutique ; tappez----> __oui__"\
          "\nPour partir de la boutique ; tappez -----------> __non__\n").lower()
    
    #Si l'utilisateur veut continuer le programme alors on continue ! :)
    if arret_continuation == "oui" :
         choisir_sa_boutique()
    
    #Si l'utilisateur ne veut pas continuer le programme alors on l'arrête :(
    elif arret_continuation == "non" :
        print ("\n___Ce fut un plaisir !___"\
               "\n___Bonne continuation !!___"\
               "\n___Et que la force soit avec vous !!!___")
    
    #Si l'utilisateur ne rentre pas ce que le programme attend, alors on lui redonne une chance !
    else :
        print ("\n____Soit oui, soit non espèce de magouilleur ^^____")
        arreter_ou_continuer_le_programme()


#Cette fonction permet à l'utilisateur de choisir, elle fait partie de l'ihm
def choisir_sa_boutique():
    choix_boutique = int(input("Dans quelle boutique voudriez vous aller !? : "\
"\n pour aller dans la boutique de Fleurry et Bott ; tappez --> __1__"\
"\n pour aller  dans la boutique de Guipure ; tappez ---------> __2__"\
"\n pour aller dans la boutique de Ollivender ; tappez -------> __3__ \n"))
    
    #Si l'utilisateur veut aller chez Fleury et Bott alors on lance la fonction liée à celle-ci et après cela on lui demande si il veut continuer
    if choix_boutique == 1 :
        fleury_bott_automatique([0, 60, 63, 231, 899])
        arreter_ou_continuer_le_programme()
        
    #Si l'utilisateur veut aller chez Guipure alors on lance la fonction liée à celle-ci et après cela on lui demande si il veut continuer
    elif choix_boutique == 2 : 
        guipure()
        arreter_ou_continuer_le_programme()
        
    #Si l'utilisateur veut aller chez Ollivender alors on lance la fonction liée à celle-ci et après cela on lui demande si il veut continuer
    elif choix_boutique == 3 :
        ollivender()
        arreter_ou_continuer_le_programme()
    
    #Si l'utilisateur ne rentre pas ce que le programme attend, alors on lui redonne une chance !
    else : 
        print ("\n___tappe soit 1 | 2 | 3 ___"\
           "\n ___et essaye pas de faire crash le programme :/ ___")
        choisir_sa_boutique()

test=True
choisir_sa_boutique()
