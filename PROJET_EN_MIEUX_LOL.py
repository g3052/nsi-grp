fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]
MAXIMUM = 4
def calcul(malle, choix):
    pesee = 0
    for element in malle:
        pesee += element[choix]
    return pesee
def malle(fournitures, poids_max):
    malle = []
    poids_malle = 0
    for objet in fournitures:
        if (poids_malle + objet['Poids']) <= poids_max: 
            poids_malle += objet['Poids']
            malle.append(objet)
    return malle
def tri(fournitures, choix):
    for i in range(len(fournitures) -1):
        mini = i
        for j in range(i+1, len(fournitures)):
            if fournitures[j][choix] > fournitures[mini][choix]:
                mini = j
        fournitures[i], fournitures[mini] = fournitures[mini], fournitures[i]
    return fournitures
print(malle(fournitures_scolaires, MAXIMUM))
print(calcul(malle(fournitures_scolaires, MAXIMUM), 'Poids'))
print(malle(tri(fournitures_scolaires, 'Poids'), MAXIMUM))
print(calcul(malle(tri(fournitures_scolaires, 'Poids'), MAXIMUM), 'Poids'))
print(malle(tri(fournitures_scolaires, 'Mana'), MAXIMUM))
print(calcul(malle(tri(fournitures_scolaires, 'Mana'), MAXIMUM), 'Mana'))
print(calcul(malle(tri(fournitures_scolaires, 'Mana'), MAXIMUM), 'Poids'))