# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 20:21:32 2021

@author: pheni
"""


def fleury_bott(somme_a_rendre):
    caisse = [500, 200, 100, 50, 20, 10, 5, 2, 1]

    # somme_a_rendre = int(input("Quelle valeur doit-on rendre ? : "))
    if somme_a_rendre > 0:

        for i in range(len(caisse)):

            if somme_a_rendre >= caisse[i]:
                print(
                    f'Il faut rendre {somme_a_rendre // caisse[i]} billet(s) de {caisse[i]}')
                somme_a_rendre = somme_a_rendre % caisse[i]

        return somme_a_rendre

    else:
        print("\nLa somme doit être supérieure à 0 ")


def fleury_bott_automatique(somme_test):

    test_1 = input("Voulez vous tester les sommes prédéfinies ? ")
    if test_1 == 'oui':
        for somme_a_rendre in somme_test:
            print("")
            print(f"Pour une somme à rendre de {somme_a_rendre} € :")
            print("")
            fleury_bott(somme_a_rendre)
        somme_a_rendre_choix = int(input("Quelle valeur voulez vous rendre ? : "))
        fleury_bott(somme_a_rendre_choix)
    else:
        somme_a_rendre = int(input("Quelle valeur doit-on rendre ? : "))
        fleury_bott(somme_a_rendre)


def guipure(somme_a_rendre1):
    '''
    Renvoi la valeur des billets à rendre selon le choix de l'utilisateur et
    le nombre de billet dans la caisse
    Entrée : None
    Sortie : Valeurs et nombre de billets à rendre
    '''

    caisse_guipure = {200: 1, 100: 3, 50: 1, 20: 1, 10: 1, 2: 5}
    
    #somme_a_rendre1 = int(input("Quelle valeur voulez vous rendre ? "))
    if somme_a_rendre1 <= 0:
        print('La somme voulue est inférieure à 0, veuillez réssayer')   
    if somme_a_rendre1 > 590:
        print("La somme est trop grande pour la caisse")
    else:
        for piece in caisse_guipure.keys():
            if somme_a_rendre1 < 0:
                break
            else:
                entier = somme_a_rendre1 // piece
                if entier > caisse_guipure[piece]:
                    print(f"La somme rendue est : {caisse_guipure[piece]} billet(s) de {piece}")
                    somme_a_rendre1 -= caisse_guipure[piece] * piece
                else:
                    print(f'La somme rendue est : {entier} billet(s) de {piece}')
                    somme_a_rendre1 = somme_a_rendre1 % piece

def guipure_automatique(somme_test_2):

    test_2 = input("Voulez vous tester les sommes prédéfinies ? ")
    if test_2 == 'oui':
        for somme_a_rendre_2 in somme_test_2:
            print("")
            print(f"Pour une somme à rendre de {somme_a_rendre_2} € :")
            print("")
            guipure(somme_a_rendre_2)
        somme_a_rendre1 = int(input("Quelle valeur doit-on rendre ? : "))
        guipure(somme_a_rendre1)
    if test_2 != 'oui':
        somme_a_rendre1 = int(input("Quelle valeur doit-on rendre ? : "))
        guipure(somme_a_rendre1)


def ollivender(gallions, mornilles, noises):
    monnaie_gallions = int(
        input("Combien de gallions voulez vous rendre ? : "))
    if type(monnaie_gallions) != type(int):
        monnaie_mornilles = int(
            input("Combien de mornilles voulez vous rendre ? : "))
    else:
        print("Attention ! Vous devez rentrer un nombre compris entre 0 et + l'infini !")
    if monnaie_mornilles >= 0:
        monnaie_noises = int(
            input("Combien de noises voulez vous rendre ? : "))
    else:
        print("Attention ! Vous devez rentrer un nombre compris entre 0 et + l'infini !")
    if monnaie_noises >= 0:
        while monnaie_noises >= 29:
            monnaie_mornilles += 1
            monnaie_noises -= 29
        while monnaie_mornilles >= 17:
            monnaie_gallions += 1
            monnaie_mornilles -= 17
    else:
        print("Attention ! Vous devez rentrer un nombre compris entre 0 et + l'infini !")

    print(f"{monnaie_gallions} gallions")
    print(f"{monnaie_mornilles} mornilles")
    print(f"{monnaie_noises} noises")


def ollivender_automatique(gallions, mornilles, noises):

    test_3 = input("Voulez vous tu tester les sommes prédéfinies ? ")
    if test_3 == 'oui':
        for i in range(len(noises)):
            print("")
            print(f"Pour une somme à rendre de {gallions[i]} gallions, de {mornilles[i]} mornilles et de {noises[i]} noises :")
            print("")
            if noises[i]>= 0:
                while noises[i] >= 29:
                    mornilles[i] += 1
                    noises[i] -= 29
                while mornilles[i] >= 17:
                    gallions[i] += 1
                    mornilles[i] -= 17

            print(f"{gallions[i]} gallions")
            print(f"{mornilles[i]} mornilles")
            print(f"{noises[i]} noises")
        ollivender(gallions, mornilles, noises)
    if test_3 != 'oui':
        ollivender(gallions, mornilles, noises)

def arreter_ou_continuer_le_programme():

    '''
    Permet à l'utilisateur de choisir si il veut continuer ou arrêter
    le programme, elle fait partie de l'ihm
    Entrée : None
    Sortie : Permet de continuer ou d'arrêter le programme en fonction des
    choix de l'utilisateur
    '''

    arret_continuation= input("Voulez aller dans une autre boutique ?"
    "\nPour aller dans une autre boutique ; tapez----> 'oui'"
    "\nPour partir de la boutique ; tapez -----------> 'non'\n").lower()

    if arret_continuation == "oui":

        '''
        Si l'utilisateur veut continuer le programme alors on continue !
        '''

        choisir_sa_boutique()

    elif arret_continuation == "non":
        '''
        Si l'utilisateur ne veut pas continuer le programme alors on l'arrête
        '''
        print("\nCe fut un paisir !"
               "\nBonne continuation dans le chemin de traverse")

    else:
        '''
        Si l'utilisateur ne rentre pas ce que le programme attend,
        alors on lui redonne une chance !
        '''
        print("\n____Soit oui, soit non espèce de magouilleur ^^____")
        arreter_ou_continuer_le_programme()


def choisir_sa_boutique():

    '''
    Permet à l'utilisateur de choisir la boutique où il veut aller,
    elle fait partie de l'ihm
    Entrée : None
    Sortie : Renvoie la boutique en fonction du choix de l'utilisateur
    '''

    choix_boutique= int(input("Dans quelle boutique voudriez vous aller !? : "
    "\nPour aller dans la boutique de Fleurry Bott ; tapez --> 1"
    "\nPour aller  dans la boutique de Guipure ; tapez ---------> 2"
    "\nPour aller dans la boutique de Ollivender ; tapez -------> 3 \n"))

    if choix_boutique == 1:
        '''
        Si l'utilisateur veut aller chez Fleury et Bott alors on lance
        la fonction liée à celle-ci et après cela on lui demande si il
        veut continuer
        '''
        fleury_bott_automatique([0, 60, 63, 231, 899])
        arreter_ou_continuer_le_programme()


    elif choix_boutique == 2:
        '''
         Si l'utilisateur veut aller chez Guipure alors on lance la fonction
         liée à celle-ci et après cela on lui demande si il veut continuer
        '''
        guipure_automatique([0, 8, 62, 231, 497, 842])
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 3:
        '''

        Si l'utilisateur veut aller chez Ollivender alors on lance la fonction
        liée à celle-ci et après cela on lui demande si il veut continuer
        '''
        gallions= [0, 0, 0, 2, 7]
        mornilles= [0, 0, 23, 11, 531]
        noises= [0, 654, 78, 9, 451]
        ollivender_automatique(gallions, mornilles, noises)
        arreter_ou_continuer_le_programme()

    else:
        '''
         Si l'utilisateur ne rentre pas ce que le programme attend,
         alors on lui redonne une chance !
        '''

        print("\n  Tappe soit 1 | 2 | 3 "
               "\n   Et essaye pas de faire crash le programme :/ ")
        choisir_sa_boutique()

choisir_sa_boutique()
