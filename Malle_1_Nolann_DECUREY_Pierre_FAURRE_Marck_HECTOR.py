# -*- coding: utf-8 -*-
"""
Programme permettant de ranger différentes malles, en fonction du poids
ou du mana. Il affiche les objets présents dans les malles avec leurs 
poids ou mana correspondant.
Auteur : Nolann DECUREY, Pierre FAURE, Mark HECTOR
"""

fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

maximum = 4


def calcul_poids_malle(malle):
    '''
    Permet de calculer le poids de la malle qu'on veut
    Entrée : Malle qu'on veut peser
    Sortie : Addition de l'ensemble des poids des objets dans la malle
    '''
    poids = 0
    
    for element in malle:
        poids += element['Poids']
    return poids


def calcul_mana_malle(malle):
    '''
    Permet de calculer le mana de la malle qu'on veut
    Entrée : Malle qu'on veut peser en mana
    Sortie : Addition de l'ensemble du mana des objets dans la malle
    '''
    mana = 0
    for element in malle:
        mana += element['Mana']
    return mana


def malle_1(fournitures):
    '''
    Renvoi une liste de dictionnaire, a l'intérieur il y a les premiers
    objets de la malle sans dépasser le poids maximum (4 kg)
    Entrée : Les fournitures scolaires
    Sortie : Liste de dictionnaire avec les premiers objets des
    fournitures scolaires
    '''
    malle = []
    poids_malle = 0
    for objet in fournitures:
        if (poids_malle + objet['Poids']) <= maximum:
            poids_malle += objet['Poids']
            malle.append(objet)
    return malle


def tri(fourniture, choix):
    '''
    Permet de trier les fournitures scolaires en fonction de leur poids de
    manière décroissante
    Entrée : Les fournitures scolaires
    Sortie : Renvoie la liste de dictionnaire  en fonction du poids
    '''
    for i in range(len(fournitures_scolaires) - 1):
        indice_du_mini = i
        for j in range(i + 1, len(fournitures_scolaires)):
            if fournitures_scolaires[j][choix] > fournitures_scolaires[indice_du_mini][choix]:
                indice_du_mini = j
        fournitures_scolaires[i], fournitures_scolaires[indice_du_mini] = fournitures_scolaires[indice_du_mini], fournitures_scolaires[i]
    return fournitures_scolaires


def malle_2(fournitures):
    '''
    Renvoi une liste de dictionnaire qui a été préalablement triée par
    la fonction tri_poids. Elle renvoie les objets
    de la malle rangé de façon optimisée pour approcher le plus possible
    de 4 kg
    Entrée : Les fournitures scolaires
    Sortie : Renvoie une liste de dictionnaire des objets qui
    permettent d'atteindre un poids maximal
    '''
    fournitures = tri(fournitures_scolaires, 'Poids')
    malle = []
    poids_malle = 0
    for objet in fournitures:
        if (poids_malle + objet['Poids']) <= maximum:
            poids_malle += objet['Poids']
            malle.append(objet)

    return malle


def malle_3(fournitures, maximum):
    '''
    Renvoi une liste de dictionnaire qui a été préalablement triée par
    la fonction tri_mana. Elle renvoie les objets de la malle rangé de
    façon optimisée pour avoir le taux de mana le plus élevé possible
    sans pour autant dépasser le poids maximal.
    Entrée : Les fournitures scolaires
    Sortie : Renvoie une liste de dictionnaire des objets qui
    permettent d'atteindre un mana maximal'
    '''
    fournitures = tri(fournitures_scolaires, 'Mana')
    malle = []
    for element in fournitures:
        if maximum > element['Poids']:
            maximum -= element['Poids']
            malle.append(element)
    return malle


def ihm():
    '''
    Permet à l'utilisateur de choisir la malle qu'il veut voir
    Entrée : Pas d'ntrée
    Sortie : Renvoie la malle voulue en fonction du choix de l'utilisateur
    '''

    choix_malle_utilisateur = int(input("Quelle malle voulez vous ouvrir ?"
                                        "\n"
                                        "\nTappez 1 pour ouvrir la malle remplie"
                                        "\nn'importe comment "
                                        "\n"
                                        "\nTappez 2 pour ouvrir la malle remplie"
                                        "\nau maximum "
                                        "\n"
                                        "\nTappez 3 pour ouvrir la malle faite"
                                        "\nen fonction du mana "
                                        "\n"
                                        "\nTappez 4 pour sortir du programme"
                                        "\n"
                                        "\nVotre choix : "))
    if choix_malle_utilisateur == 1:
        print('')
        print("Voici les objets de la malle d'Harry remplie n'importe comment :")
        print('')
        malle_harry_remplie_hasard = malle_1(fournitures_scolaires)
        for element in malle_harry_remplie_hasard:
            objet_de_la_malle = element['Nom']
            poids_de_objet = element['Poids']
            print(f"{objet_de_la_malle} ---> {poids_de_objet} kg")
        poids_malle_remplie_hasard = calcul_poids_malle(malle_harry_remplie_hasard)
        print('Le poids total de la malle'
              f'\nest {poids_malle_remplie_hasard} kg')
        ihm()

    elif choix_malle_utilisateur == 2:
        print('')
        print("Voici les objets de la malle d'Harry remplie au maximum :")
        print('')

        malle_harry_remplie_poids_maximum = malle_2(fournitures_scolaires)
        for element in malle_harry_remplie_poids_maximum:
            objet_de_la_malle = element['Nom']
            poids_de_objet = element['Poids']
            print(f"{objet_de_la_malle} ---> {poids_de_objet} kg")
        poids_malle_remplie_parfaitement = calcul_poids_malle(malle_harry_remplie_poids_maximum)
        print('Le poids total de la malle completement remplie'
              f"\nest de {poids_malle_remplie_parfaitement} kg")
        print('')
        ihm()

    elif choix_malle_utilisateur == 3:
        print('')
        print("Voici les objets de la malle d'Harry remplie au maximum "
              "\navec le mana:")
        print('')
        malle_harry_remplie_mana_maximum = malle_3(fournitures_scolaires, maximum)
        for element in malle_harry_remplie_mana_maximum:
            objet_de_la_malle = element['Nom']
            mana_de_objet = element['Mana']
            print(f"{objet_de_la_malle} ---> {mana_de_objet}")
        poids_malle_mana = calcul_poids_malle(malle_harry_remplie_mana_maximum)
        print(f"Le poids de la malle est de {poids_malle_mana} kg")
        mana_calcul_remplie_maximum = calcul_mana_malle(malle_harry_remplie_mana_maximum)
        print(f'La mana total de la malle est {mana_calcul_remplie_maximum}')
        print('')
        ihm()
    else:
        print("")
        print("A bientôt pour de nouvelles malles :) !")

ihm()
