# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 18:39:28 2021

@author: faure
"""

def fleury_bott(somme_a_rendre):
    caisse = [500, 200 , 100, 50, 20, 10, 5, 2, 1]

    
    #somme_a_rendre = int(input("Quelle valeur doit-on rendre ? : "))
    if somme_a_rendre > 0:
        
        for i in range(len(caisse)):
            
            if somme_a_rendre >= caisse[i]:
                print(f'  Il faut rendre {somme_a_rendre // caisse[i]} billet(s) de {caisse[i]}')
                somme_a_rendre = somme_a_rendre % caisse[i]
        
        return somme_a_rendre

    else:
            print("\n  La somme doit être supérieure à 0 ")

    


def fleury_bott_automatique(somme_test) : 
  
    for somme_a_rendreu in somme_test:
        print(f"Pour une somme à rendre de {somme_a_rendreu} € :")
        fleury_bott(somme_a_rendreu)
    somme_a_rendre_choix = int(input("Quelle valeur voulez vous rendre ? : "))
    fleury_bott(somme_a_rendre_choix)
    

def guipure(somme_a_rendre1):
    '''
    Renvoi la valeur des billets à rendre selon le choix de l'utilisateur et
    le nombre de billet dans la caisse
    Entrée : None
    Sortie : Valeurs et nombre de billets à rendre
    '''

    caisse_guipure = [[200, 1], [100, 3], [50, 1], [20, 1], [10, 1], [2, 5]]
    valeurs_billets = [[200, 0], [100, 0], [50, 0], [20, 0], [10, 0], [2, 5]]
    

    if somme_a_rendre1 == 0:
        print('\n  La somme doit être supérieure à 0')

    if somme_a_rendre1 >= 590:
        print("\n  La somme ne doit pas être supérieure à 590 euros")
    elif somme_a_rendre1 < 0:
        print('\n  La somme ne doit pas être inférieure à 0')
    else:

        for j in range(len(caisse_guipure)):
            i = caisse_guipure[j]
            if somme_a_rendre1 >= i[0]:
                ent = somme_a_rendre1 // i[0]
                if ent > i[1]:
                    print(f"rendre {i[1]} billets de {i[0]}€")
                    somme_a_rendre1 = somme_a_rendre1-i[1] * i[0]
                    valeurs_billets[j][1] += i[1]
                else:
                    print(f"rendre {i[1]} billets de {i[0]}€")
                    somme_a_rendre1 = somme_a_rendre1-ent * i[0]
                    valeurs_billets[j][1] += ent
    
        if somme_a_rendre1 != 0:
            print("Somme rendue différente")
        return valeurs_billets

    
    


def guipure_automatique(somme_test) : 
  
    for somme_a_rendreux in somme_test:
        print(f"Pour une somme à rendre de {somme_a_rendreux} € :")
        guipure(somme_a_rendreux)
    somme_a_rendre_choixg = int(input("Quelle valeur voulez vous rendre ? : "))
    guipure(somme_a_rendre_choixg)

def ollivender(monnaie_gallions = 0, monnaie_mornilles = 0, monnaie_noises = 0):

    
    
    while monnaie_noises >= 29 :
            monnaie_mornilles += 1 
            monnaie_noises -= 29
    while monnaie_mornilles >= 17 :
            monnaie_gallions += 1
            monnaie_mornilles -= 17
    print ("\n Nous allons donc rendre : ")
    print (f"  {monnaie_gallions} gallions")
    print (f"  {monnaie_mornilles} mornilles")
    print (f"  {monnaie_noises} noises")
    
def ollivender_automatique(somme_test_gallions, somme_test_mornilles, somme_test_noises) : 
  
    for somme_test in somme_test_gallions, somme_test_mornilles, somme_test_noises:
        print(f"Pour une somme à rendre de {somme_test_gallions} gallions, de {somme_test_mornilles} mornilles et de {somme_test_noises} noises :")
        ollivender(somme_test)
    monnaie_gallions = int(input("Combien de gallions voulez vous rendre ? : "))
    monnaie_mornilles = int(input("Combien de mornilles voulez vous rendre ? : "))
    monnaie_noises = int(input("Combien de noises voulez vous rendre ? : "))
    ollivender(monnaie_gallions, monnaie_mornilles, monnaie_noises )    

def arreter_ou_continuer_le_programme():

    '''
    Permet à l'utilisateur de choisir si il veut continuer ou arrêter
    le programme, elle fait partie de l'ihm
    Entrée : None
    Sortie : Permet de continuer ou d'arrêter le programme en fonction des
    choix de l'utilisateur
    '''

    arret_continuation = input("Voulez aller dans une autre boutique ?"
    "\nPour aller dans une autre boutique ; tapez----> 'oui'"
    "\nPour partir de la boutique ; tapez -----------> 'non'\n").lower()

    if arret_continuation == "oui":

        '''
        Si l'utilisateur veut continuer le programme alors on continue !
        '''

        choisir_sa_boutique()

    elif arret_continuation == "non":
        '''
        Si l'utilisateur ne veut pas continuer le programme alors on l'arrête
        '''
        print ("\nCe fut un paisir !"
               "\n Bonne continuation !!"
               "\n  Et que la force soit avec vous !!!")

    else:
        '''
        Si l'utilisateur ne rentre pas ce que le programme attend,
        alors on lui redonne une chance !
        '''
        print ("\n____Soit oui, soit non espèce de magouilleur ^^____")
        arreter_ou_continuer_le_programme()


def choisir_sa_boutique():

    '''
    Permet à l'utilisateur de choisir la boutique où il veut aller,
    elle fait partie de l'ihm
    Entrée : None
    Sortie : Renvoie la boutique en fonction du choix de l'utilisateur
    '''

    choix_boutique = int(input("Dans quelle boutique voudriez vous aller !? : "
    "\nPour aller dans la boutique de Fleurry Bott ; tapez --> 1"
    "\nPour aller  dans la boutique de Guipure ; tapez ---------> 2"
    "\nPour aller dans la boutique de Ollivender ; tapez -------> 3 \n"))

    if choix_boutique == 1:

        '''
        Si l'utilisateur veut aller chez Fleury et Bott alors on lance
        la fonction liée à celle-ci et après cela on lui demande si il
        veut continuer
        '''
        fleury_bott_automatique([0, 60, 63, 231, 899])
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 2:
        '''
         Si l'utilisateur veut aller chez Guipure alors on lance la fonction
         liée à celle-ci et après cela on lui demande si il veut continuer
        '''

        guipure_automatique([0, 8, 62, 231, 497, 842])
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 3:
        '''

        Si l'utilisateur veut aller chez Ollivender alors on lance la fonction
        liée à celle-ci et après cela on lui demande si il veut continuer
        '''

        ollivender_automatique(45, 67, 56)
        arreter_ou_continuer_le_programme()

    else:
        '''
         Si l'utilisateur ne rentre pas ce que le programme attend,
         alors on lui redonne une chance !
        '''

        print ("\n  Tappe soit 1 | 2 | 3 "
               "\n   Et essaye pas de faire crash le programme :/ ")
        choisir_sa_boutique()


choisir_sa_boutique()
