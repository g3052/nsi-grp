# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 10:31:51 2021

@author: DECUREY
"""


def fleury_bott():
    '''
    Crée une fonction permettant d'avoir des valeurs dans une liste, de
    comparé la monnaie entrée par l'utilisateur avec l'argent de la caisse et
    de rendre le bon nombre de billets
    Entrée : None
    Sortie : Rend le bon nombre de billets en fonction de la valeur rentrée
    par l'utilisateur
    '''

    caisse = [500, 200, 50, 20, 10, 5, 2, 1]
    monnaie = int(input("Quelle valeur doit-on rendre ? : "))
    if monnaie > 0:
        for i in range(len(caisse)):
            if monnaie >= caisse[i]:
                print(f"\nIl faut rendre {monnaie // caisse[i]} billet(s) de {caisse[i]}")
                monnaie = monnaie % caisse[i]
        return monnaie
    else:
        print("La somme doit être supérieure à 0")

    fleury_bott()


def guipure():
    '''
    Renvoi la valeur des billets à rendre selon le choix de l'utilisateur et
    le nombre de billet dans la caisse
    Entrée : None
    Sortie : Valeurs et nombre de billets à rendre
    '''
    caisse_2 = {200: 1, 100: 3, 50: 1, 20: 1, 10: 1, 2: 5}
    monnaie_2 = int(input("Quelle valeur doit-on rendre ? : "))

    if monnaie_2 == 0:
            print('\n__La somme doit être supérieure à 0__')

    for valeur_billets in caisse_2.keys():
        if monnaie_2 >= 590:
            print("\n__La somme ne doit pas être supérieure à 590 euro__")
            break
        elif monnaie_2 < 0:
            print('\n__La somme ne doit pas être inférieure à 0__')
            break
        elif monnaie_2 == 0:
            break
        else:
            print(f"\nIl faut rendre {monnaie_2 // valeur_billets} billet(s) de {valeur_billets}")
            monnaie_2 = monnaie_2 % valeur_billets

        if monnaie_2 % valeur_billets == 1:
            print('La somme est rendue est différente')

    return valeur_billets

    guipure()


def ollivender():
    print ("là il faut mettre le code pour la boutique d ollivender")


def arreter_ou_continuer_le_programme():

    '''
    Permet à l'utilisateur de choisir si il veut continuer ou arrêter
    le programme, elle fait partie de l'ihm
    Entrée : None
    Sortie : Permet de continuer ou d'arrêter le programme en fonction des
    choix de l'utilisateur
    '''

    arret_continuation = input("Voulez aller dans une autre boutique ?"
    "\nPour aller dans une autre boutique ; tappez----> 'oui'"
    "\nPour partir de la boutique ; tappez -----------> 'non'\n").lower()

    if arret_continuation == "oui":

        '''
        Si l'utilisateur veut continuer le programme alors on continue !
        '''

        choisir_sa_boutique()

    elif arret_continuation == "non":
        '''
        Si l'utilisateur ne veut pas continuer le programme alors on l'arrête
        '''
        print ("\nCe fut un paisir !"
               "\nBonne continuation !!"
               "\nEt que la force soit avec vous !!!")

    else:
        '''
        Si l'utilisateur ne rentre pas ce que le programme attend,
        alors on lui redonne une chance !
        '''
        print ("\n____Soit oui, soit non espèce de magouilleur ^^____")
        arreter_ou_continuer_le_programme()


def choisir_sa_boutique():

    '''
    Permet à l'utilisateur de choisir la boutique où il veut aller,
    elle fait partie de l'ihm
    Entrée : None
    Sortie : Renvoie la boutique en fonction du choix de l'utilisateur
    '''

    choix_boutique = int(input("Dans quelle boutique voudriez vous aller !? : "
    "\nPour aller dans la boutique de Fleurry Bott ; tappez --> '1'"
    "\nPour aller  dans la boutique de Guipure ; tappez ---------> '2'"
    "\nPour aller dans la boutique de Ollivender ; tappez -------> '3' \n"))

    if choix_boutique == 1:

        '''
        Si l'utilisateur veut aller chez Fleury et Bott alors on lance
        la fonction liée à celle-ci et après cela on lui demande si il
        veut continuer
        '''

        fleury_bott()
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 2:
        '''
         Si l'utilisateur veut aller chez Guipure alors on lance la fonction
         liée à celle-ci et après cela on lui demande si il veut continuer
        '''

        guipure()
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 3:
        '''

        Si l'utilisateur veut aller chez Ollivender alors on lance la fonction
        liée à celle-ci et après cela on lui demande si il veut continuer
        '''

        ollivender()
        arreter_ou_continuer_le_programme()

    else:
        '''
         Si l'utilisateur ne rentre pas ce que le programme attend,
         alors on lui redonne une chance !
        '''

        print ("\n___tappe soit 1 | 2 | 3 ___"
               "\n ___et essaye pas de faire crash le programme :/ ___")
        choisir_sa_boutique()


choisir_sa_boutique()
