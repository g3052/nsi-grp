# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 12:12:22 2021

@author: FAUREP
"""


def fleury_bott():
    '''
    Crée une fonction permettant d'avoir des valeurs dans une liste, de
    comparé la monnaie entrée par l'utilisateur avec l'argent de la caisse et
    de rendre le bon nombre de billets
    Entrée : None
    Sortie : Rend le bon nombre de billets en fonction de la valeur rentrée
    par l'utilisateur
    '''

    caisse = [500, 200, 50, 20, 10, 5, 2, 1]
    monnaie = int(input("Quelle valeur doit-on rendre ? : "))
    if monnaie > 0:
        for i in range(len(caisse)):
            if monnaie >= caisse[i]:
                print(f"\nIl faut rendre {monnaie // caisse[i]} billet(s) de {caisse[i]}")
                monnaie = monnaie % caisse[i]
        return monnaie
    else:
        print("La somme doit être supérieure à 0")

    fleury_bott()


def guipure():
    '''
    Renvoi la valeur des billets à rendre selon le choix de l'utilisateur et
    le nombre de billet dans la caisse
    Entrée : None
    Sortie : Valeurs et nombre de billets à rendre
    '''

    caisse_guipure=[[200,1],[100,3],[50,1],[20,1],[10,1],[2,5]]
    valeurs_billets=[[200,0],[100,0],[50,0],[20,0],[10,0],[2,5]]
    monnaie_2 = int(input("Quelle valeur doit-on rendre ? : "))
    
    
    if monnaie_2 == 0:
            print('\n  La somme doit être supérieure à 0')
            
    
    if monnaie_2 >= 590:
      print("\n  La somme ne doit pas être supérieure à 590 euros")
    elif monnaie_2 <0:
      print('\n  La somme ne doit pas être inférieure à 0')
    else:
      
      for j in range(len(caisse_guipure)):
        i=caisse_guipure[j]
        if monnaie_2>=i[0]:
           ent=monnaie_2//i[0]
           if ent>i[1]:
              print("rendre %3.0i billets de %3.0f €"%(i[1],i[0]))
              monnaie_2=monnaie_2-i[1]*i[0]
              valeurs_billets[j][1]+=i[1]
           else:
              print("rendre %3.0i billets de %3.0f €"%(ent, i[0]))
              monnaie_2=monnaie_2-ent*i[0]
              valeurs_billets[j][1]+=ent
            


    if monnaie_2!=0:
      print("pas assez de billets")  
    return valeurs_billets
            
    guipure()


def ollivender():
    monnaie_gallions = int(input("Combien de gallions voulez vous rendre ? : "))
    if type(monnaie_gallions) != type(int) :
        monnaie_mornilles = input("Combien de mornilles voulez vous rendre ? : ")
    else : 
        print("Attention ! Vous devez rentrer un nombre compris entre 0 et + l'infini !")
    if monnaie_mornilles >=  0 :    
        monnaie_noises = input("Combien de noises voulez vous rendre ? : ")
    else : 
        print("Attention ! Vous devez rentrer un nombre compris entre 0 et + l'infini !")
    if monnaie_noises >= 0 : 
        while monnaie_noises >= 29 :
            monnaie_mornilles += 1 
            monnaie_noises -= 29
        while monnaie_mornilles >= 17 :
            monnaie_gallions += 1
            monnaie_mornilles -= 17
    else : 
        print("Attention ! Vous devez rentrer un nombre compris entre 0 et + l'infini !")
   
    print (f"{monnaie_gallions} gallions")
    print (f"{monnaie_mornilles} mornilles")
    print (f"{monnaie_noises} noises")
    
    
    ollivender()

#monnaie_gallions = input("Combien de gallions voulez vous rendre ? : ")
#if type(monnaie_gallions) != type(int) :
 #   print ("ntm")
#else : 
 #   if monnaie_gallions <= 0 :
def arreter_ou_continuer_le_programme():

    '''
    Permet à l'utilisateur de choisir si il veut continuer ou arrêter
    le programme, elle fait partie de l'ihm
    Entrée : None
    Sortie : Permet de continuer ou d'arrêter le programme en fonction des
    choix de l'utilisateur
    '''

    arret_continuation = input("Voulez aller dans une autre boutique ?"
    "\nPour aller dans une autre boutique ; tapez----> 'oui'"
    "\nPour partir de la boutique ; tapez -----------> 'non'\n").lower()

    if arret_continuation == "oui":

        '''
        Si l'utilisateur veut continuer le programme alors on continue !
        '''

        choisir_sa_boutique()

    elif arret_continuation == "non":
        '''
        Si l'utilisateur ne veut pas continuer le programme alors on l'arrête
        '''
        print ("\nCe fut un paisir !"
               "\n Bonne continuation !!"
               "\n  Et que la force soit avec vous !!!")

    else:
        '''
        Si l'utilisateur ne rentre pas ce que le programme attend,
        alors on lui redonne une chance !
        '''
        print ("\n____Soit oui, soit non espèce de magouilleur ^^____")
        arreter_ou_continuer_le_programme()


def choisir_sa_boutique():

    '''
    Permet à l'utilisateur de choisir la boutique où il veut aller,
    elle fait partie de l'ihm
    Entrée : None
    Sortie : Renvoie la boutique en fonction du choix de l'utilisateur
    '''

    choix_boutique = int(input("Dans quelle boutique voudriez vous aller !? : "
    "\nPour aller dans la boutique de Fleurry Bott ; tapez --> 1"
    "\nPour aller  dans la boutique de Guipure ; tapez ---------> 2"
    "\nPour aller dans la boutique de Ollivender ; tapez -------> 3 \n"))

    if choix_boutique == 1:

        '''
        Si l'utilisateur veut aller chez Fleury et Bott alors on lance
        la fonction liée à celle-ci et après cela on lui demande si il
        veut continuer
        '''

        fleury_bott()
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 2:
        '''
         Si l'utilisateur veut aller chez Guipure alors on lance la fonction
         liée à celle-ci et après cela on lui demande si il veut continuer
        '''

        guipure()
        arreter_ou_continuer_le_programme()

    elif choix_boutique == 3:
        '''

        Si l'utilisateur veut aller chez Ollivender alors on lance la fonction
        liée à celle-ci et après cela on lui demande si il veut continuer
        '''

        ollivender()
        arreter_ou_continuer_le_programme()

    else:
        '''
         Si l'utilisateur ne rentre pas ce que le programme attend,
         alors on lui redonne une chance !
        '''

        print ("\n  Tappe soit 1 | 2 | 3 "
               "\n   Et essaye pas de faire crash le programme :/ ")
        choisir_sa_boutique()


choisir_sa_boutique()
